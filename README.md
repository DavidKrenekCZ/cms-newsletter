# Newsletter CMS module

## Installation

- Require to composer
``` json
composer require composer require davidkrenekcz/newsletter
```

- Run migrations
``` json
php artisan module:migrate Newsletter
```

- Add these providers to _config\app.php_ at the end of array 'providers'
``` php
	'providers' => [
		/* ... current providers ...*/

		Modules\Newsletter\Providers\NewsletterInitProvider::class,
		Fedeisas\LaravelMailCssInliner\LaravelMailCssInlinerServiceProvider::class,
    	Spatie\Newsletter\NewsletterServiceProvider::class,
    ],
``` 

- Publish config files, then edit important variables in _config/dk-newsletter.php_
``` json
php artisan vendor:publish --provider="Modules\Newsletter\Providers\NewsletterServiceProvider"
php artisan vendor:publish --provider="Spatie\Newsletter\NewsletterServiceProvider"
```

## Frontend usage of form
Include form to your template
``` json
@include("newsletter::frontend.form")
```
You can use variables to define form's classes and texts, see template for more details.

You can include default AJAX form controller to your template
``` json
@include("newsletter::frontend.ajax")
```
or you can write your own.

AJAX form controller automatically sends requests to the server. You can control AJAX behaviour like this:
``` javascript
<script>
	window.newsletterAjaxEvents.onInit = function() {
			// this function runs right before AJAX request is initialized
			// you can show loader here for example
	};

	window.newsletterAjaxEvents.onDone = function() {
			// this function runs right after AJAX request ends
			// you can hide loader here for example
	};

	window.newsletterAjaxEvents.onSuccess = function() {
			// this function runs right after AJAX request ends if it was successful
			// you can alert user here that his subscription was successful
	};

	window.newsletterAjaxEvents.onFail = function(errorCode) {
			// this function runs right after AJAX request ends if it was unsuccessful
			// you can alert user here that his subscription was unsuccessful
			// there are a few error codes available:
			if (errorCode == "DB_ERROR") {
				// error with DB connection
			} else if (errorCode == "ALREADY_SUBSCRIBED") {
				// user is already subscribed
			} else if (errorCode == "INVALID_EMAIL") {
				// provided e-mail address is not valid
			} else {
				// unknown error
			}
	};
</script>
```