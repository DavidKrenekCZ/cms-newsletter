<?php

namespace Modules\Newsletter\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class QueueMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $content;
    protected $mailSubject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $content)
    {
        $this->mailSubject = $subject;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config("dk-newsletter.sendingMail"), config("dk-newsletter.sendingName"))
                ->subject($this->mailSubject)
                ->view('newsletter::mail.mail')
                ->with([
                    "mailContent" => $this->content
                ]);
    }
}
