<?php

namespace Modules\Newsletter\Entities;

use Illuminate\Database\Eloquent\Model;

class QueueTask extends Model {
    public $timestamps = false;

    protected $table = "newsletter__queue_tasks";

    public static function nextId() {
    	$task = QueueTask::where("in_progress", 0)->first();
    	if (!$task)
    		return false;
    	return $task->id;
    }
}
