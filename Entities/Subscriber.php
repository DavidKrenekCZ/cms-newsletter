<?php

namespace Modules\Newsletter\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscriber extends Model
{
	use Translatable;
	use SoftDeletes;

	protected $table = 'newsletter__subscribers';
	public $translatedAttributes = [];
	protected $fillable = [];

	protected $appends = ["categoriesIds", "categories", "categoriesNames"];

	public function getCategoriesIdsAttribute() {
		if (trim($this->delivery_categories) != "")
			return explode(";", $this->delivery_categories);
		return [];
	}

	public function getCategoriesAttribute() {
		$result = [];
		foreach ($this->categoriesIds as $id) {
			$category = \Modules\DynamicPages\Entities\Category::find($id);
			if ($category)
				$result[] = $category;
		}
		return $result;
	}

	public function getCategoriesNamesAttribute() {
		$result = [];
		foreach ($this->categories as $category)
			$result[] = $category->name;
		return $result;
	}

	public function unsubscribe() {
		$this->unsubscribed_at = date("Y-m-d H:i:s");
		return $this->save();
	}

	public function subscribe() {
		$this->unsubscribed_at = null;
		return $this->save();
	}

	public function isSubscribed() {
		return $this->unsubscribed_at == null || $this->unsubscribed_at == "0000-00-00 00:00:00";
	}
}
