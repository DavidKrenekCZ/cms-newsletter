<?php

namespace Modules\Newsletter\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use Translatable;

    protected $table = 'newsletter__settings';
    public $translatedAttributes = [];
    protected $fillable = [];
    private static $defaults = [
    	"MAILCHIMP_APIKEY" 	=> "",
    	"MAILCHIMP_ID"		=> "",
    	"DP_ON"				=> "0",
        "DP_WEEK"           => "1",
        "DP_WEEK_TIME"      => "12:00",
        "DP_MONTH"          => "1",
        "DP_MONTH_TIME"     => "12:00",
    ];

    public static function allArray() {
    	$result = [];
    	foreach (self::all() as $item)
    		$result[$item->key] = $item->getValue();

    	foreach (self::$defaults as $index => $value)
    		if (!isset($result[$index]))
    			$result[$index] = $value;

    	return $result;
    }

    public static function get($index, $default="no_default_set") {
    	$setting = Setting::where("key", $index);
    	if ($setting && $setting->count())
    		return $setting->first()->getValue($default);

    	if ($default != "no_default_set")
    		return $default;

    	if (isset(self::$defaults[$index]))
    		return self::$defaults[$index];
    	return false;
    }

    private function getValue($default="no_default_set") {
    	if ($this->key == "MAILCHIMP_APIKEY" || $this->key == "MAILCHIMP_ID")
    		return self::censor($this->value, 5);

    	if ($this->key == "DP_ON")
    		return $this->value == "on";

    	return $this->value;
    }

    private static function censor($string, $replace=5) {
        if (mb_strlen($string) == 0)
            return "";
        $new = mb_substr($string, 0, -$replace);

        for ($x = 0; $x < $replace; $x++) {
            $new .= "*";
        }
        return $new;
    }
}
