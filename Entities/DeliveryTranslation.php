<?php

namespace Modules\Newsletter\Entities;

use Illuminate\Database\Eloquent\Model;

class DeliveryTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'newsletter__delivery_translations';
}
