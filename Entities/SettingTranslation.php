<?php

namespace Modules\Newsletter\Entities;

use Illuminate\Database\Eloquent\Model;

class SettingTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'newsletter__setting_translations';
}
