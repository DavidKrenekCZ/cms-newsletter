<?php

namespace Modules\Newsletter\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use Translatable;

    protected $table = 'newsletter__deliveries';
    public $translatedAttributes = [];
    protected $fillable = [
    	"type",
    	"sent_to",
    	"done_at"
    ];
}
