<?php

namespace Modules\Newsletter\Providers;

use Blade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class NewsletterInitProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Configure Newsletter setting from database
     * This NEEDS to be done so the configration is changed before Newsletter provider takes it and initializes itself with it
     * We have to use DB and not Eloquent because it's not ready yet
     *
     * @return void
     */
    public function register() {
        if (\Schema::hasTable('newsletter__settings')) { // just in case
            foreach (DB::table("newsletter__settings")->get() as $item) {
                if ($item->key == "MAILCHIMP_APIKEY" && trim($item->value) != "") // item is api key => set configuration
                    config(['laravel-newsletter.apiKey' => $item->value]);
                elseif ($item->key == "MAILCHIMP_ID" && trim($item->value) != "") // item is list id => set configuration
                    config(['laravel-newsletter.lists.'.config("laravel-newsletter.defaultListName").'.id' => $item->value]);
            }
        }
    }
}
