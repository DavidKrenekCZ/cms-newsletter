<?php

namespace Modules\Newsletter\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Newsletter\Events\Handlers\RegisterNewsletterSidebar;

class NewsletterServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterNewsletterSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('subscribers', array_dot(trans('newsletter::subscribers')));
            $event->load('settings', array_dot(trans('newsletter::settings')));
            $event->load('deliveries', array_dot(trans('newsletter::deliveries')));
            // append translations



        });
    }

    public function boot()
    {
        $this->publishConfig('newsletter', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        $this->publishes([
            __DIR__.'/../Config/publish/laravel-newsletter.php' => config_path('laravel-newsletter.php'),
            __DIR__.'/../Config/publish/dk-newsletter.php' => config_path('dk-newsletter.php'),
        ]);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Newsletter\Repositories\SubscriberRepository',
            function () {
                $repository = new \Modules\Newsletter\Repositories\Eloquent\EloquentSubscriberRepository(new \Modules\Newsletter\Entities\Subscriber());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Newsletter\Repositories\Cache\CacheSubscriberDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Newsletter\Repositories\SettingRepository',
            function () {
                $repository = new \Modules\Newsletter\Repositories\Eloquent\EloquentSettingRepository(new \Modules\Newsletter\Entities\Setting());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Newsletter\Repositories\Cache\CacheSettingDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Newsletter\Repositories\DeliveryRepository',
            function () {
                $repository = new \Modules\Newsletter\Repositories\Eloquent\EloquentDeliveryRepository(new \Modules\Newsletter\Entities\Delivery());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Newsletter\Repositories\Cache\CacheDeliveryDecorator($repository);
            }
        );
// add bindings



    }
}
