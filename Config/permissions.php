<?php

return [
    'newsletter.subscribers' => [
        'index' => 'newsletter::subscribers.list resource',
        'create' => 'newsletter::subscribers.create resource',
    ],
    'newsletter.settings' => [
        'mailchimp' => 'newsletter::settings.mail chimp',
        'dynamicpages' => 'newsletter::settings.dynamic pages',
    ],



];
