<?php

return [

        /*
         * Your address to send mails from
         */
        'sendingMail' => "info@example.com",

        /*
         * Your name to send mails from
         */
        'sendingName' => "John Doe",

        /*
         * Header in e-mails navigation bar
         */
        'emailNavHeader' => 'Header in navigation',
];
