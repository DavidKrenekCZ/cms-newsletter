<?php
/*
    * WARNING - DON'T EDIT THIS FILE!
    * This file only serves as config file for inner module, don't edit it!!!
*/
return [
        'apiKey' => "dummyPlaceholder",
        'defaultListName' => 'defaultList',
        'lists' => [
            'defaultList' => [
                'id' => "fooPlaceholder",
            ],
        ],
        'ssl' => true,
];