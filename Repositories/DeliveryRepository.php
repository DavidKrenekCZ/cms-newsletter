<?php

namespace Modules\Newsletter\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface DeliveryRepository extends BaseRepository
{
}
