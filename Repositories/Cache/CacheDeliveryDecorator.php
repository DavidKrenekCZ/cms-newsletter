<?php

namespace Modules\Newsletter\Repositories\Cache;

use Modules\Newsletter\Repositories\DeliveryRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheDeliveryDecorator extends BaseCacheDecorator implements DeliveryRepository
{
    public function __construct(DeliveryRepository $delivery)
    {
        parent::__construct();
        $this->entityName = 'newsletter.deliveries';
        $this->repository = $delivery;
    }
}
