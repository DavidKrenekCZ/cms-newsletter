<?php

namespace Modules\Newsletter\Repositories\Cache;

use Modules\Newsletter\Repositories\SettingRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheSettingDecorator extends BaseCacheDecorator implements SettingRepository
{
    public function __construct(SettingRepository $setting)
    {
        parent::__construct();
        $this->entityName = 'newsletter.settings';
        $this->repository = $setting;
    }
}
