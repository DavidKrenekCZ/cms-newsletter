<?php

namespace Modules\Newsletter\Repositories\Eloquent;

use Modules\Newsletter\Repositories\DeliveryRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentDeliveryRepository extends EloquentBaseRepository implements DeliveryRepository
{
}
