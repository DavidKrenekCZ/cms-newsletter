<?php

namespace Modules\Newsletter\Repositories\Eloquent;

use Modules\Newsletter\Repositories\SettingRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentSettingRepository extends EloquentBaseRepository implements SettingRepository
{
}
