<?php
return [
	"title" => [
		"newsletters" => "Hromadná pošta"
	],
	"mail-sent" => "E-mail úspěšně zařazen do fronty pro odeslání",
	"subscribe" => "Přihlásit k odběru"
];