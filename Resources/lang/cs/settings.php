<?php

return [
    "save"  => "Uložit",
    'title' => [
        'settings' => 'Nastavení',
        "mail chimp" => "MailChimp integrace",
        "dynamic pages" => "Dynamické stránky"
    ],

    "fields" => [
    	"apikey"    => "API klíč",
    	"listid"    => "ID seznamu",
    	"dynamic pages on" 		=> "Pravidelné newslettery",
    	"dynamic pages week" 	=> "Spouštění týdenního newsletteru",
    	"dynamic pages month" 	=> "Spouštění měsíčního newsletteru",
    ],

    "days" => [
    	"1" => "Pondělí",
    	"2" => "Úterý",
    	"3" => "Středa",
    	"4" => "Čtvrtek",
    	"5" => "Pátek",
    	"6" => "Sobota",
    	"7" => "Neděle",
    ],
    "day" => "den",

    "mail chimp" => "MailChimp integrace",
    "dynamic pages" => "Nastavení Dynamických stránek",
];
