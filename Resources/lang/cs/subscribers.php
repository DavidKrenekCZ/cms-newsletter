<?php

return [
    "deleted" => "Odběratel úspěšně smazán",
    "unsubscribed" => "Odběratel úspěšně odhlášen z odběru",
    "subscribed" => "Odběratel úspěšně přihlášen k odběru",
    "subscribe" => "Přihlásit k odběru",
    "unsubscribe" => "Odhlásit z odběru",    
    "updated" => "Seznam odběratelů úspěšně synchronizován",
    "update list" => "Synchronizovat seznam",
    "update list title" => "Synchronizace vytvoří průnik množiny odběratelů z tabulky níže a množiny odběratelů v tabulce MailChimpu",
    'title' => [
        'subscribers' => 'Hromadný e-mail',
        'subscribers-list' => 'Seznam odběratelů',
        "subscribed" => "Přihlášen k odběru",
        "delivery categories" => "Odebírané kategorie",
        "delivery type" => "Frekvence odběru"
    ],

    "delivery types" => [
        "always"    => "Pokaždé",
        "week"      => "Týdně",
        "month"     => "Měsíčně",
        ""          => "-"
    ],
    
    "subject" => "Předmět",
    "content" => "Obsah",

    "preview" => "Náhled",
    "send" => "Odeslat",

    'list resource' => 'Zobrazit seznam odběratelů',
    'create resource' => 'Posílat hromadné zprávy',
];
