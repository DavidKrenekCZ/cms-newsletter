<?php
return [
	"title" => [
		"newsletters" => "Newsletter"
	],
	"mail-sent" => "Mail successfully queued",
	"subscribe" => "Subscribe"
];