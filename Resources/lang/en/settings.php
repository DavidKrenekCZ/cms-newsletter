<?php

return [
    "save"  => "Save",
    'title' => [
        'settings' => 'Settings',
        "mail chimp" => "MailChimp integration",
        "dynamic pages" => "Dynamic pages"
    ],

    "fields" => [
    	"apikey"    => "API key",
    	"listid"    => "List ID",
    	"dynamic pages on" => "Regular newsletters",
    	"dynamic pages week" 	=> "Week newsletter trigger",
    	"dynamic pages month" 	=> "Month newsletter trigger",
    ],

    "days" => [
    	"1" => "Monday",
    	"2" => "Tuesday",
    	"3" => "Wednesday",
    	"4" => "Thursday",
    	"5" => "Friday",
    	"6" => "Saturday",
    	"7" => "Sunday",
    ],
    "day" => "day",

    'list resource' => 'Edit settings',
    "mail chimp" => "MailChimp integration",
    "dynamic pages" => "Dynamic Pages settings",
];
