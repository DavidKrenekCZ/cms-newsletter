<?php

return [
    'list resource' => 'List deliveries',
    'create resource' => 'Create deliveries',
    'edit resource' => 'Edit deliveries',
    'destroy resource' => 'Destroy deliveries',
    'title' => [
        'deliveries' => 'Delivery',
        'create delivery' => 'Create a delivery',
        'edit delivery' => 'Edit a delivery',
    ],
    'button' => [
        'create delivery' => 'Create a delivery',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
