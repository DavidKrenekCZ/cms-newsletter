<?php

return [
    "deleted" => "Subscriber successfully deleted",
    "unsubscribed" => "Subscriber successfully unsubscribed",
    "subscribed" => "Subscriber successfully subscribed",
    "subscribe" => "Subscribe",
    "unsubscribe" => "Unsubscribe",
    "updated" => "Subscribers list successfully updated",
    "update list" => "Update list",
    "update list title" => "Creates an intersection of a set of local database addresses and MailChimp addresses",
    'title' => [
        'subscribers' => 'Send a newsletter',
        'subscribers-list' => 'Subscribers list',
        "subscribed" => "Subscribed",
        "delivery categories" => "Subscribed categories",
        "delivery type" => "Frequency of subscription"
    ],
    
    "delivery types" => [
        "always"    => "Always",
        "week"      => "Weekly",
        "month"     => "Monthly",
        ""          => "-"
    ],

    "subject" => "Subject",
    "content" => "Content",

    "preview" => "Preview",
    "send" => "Send",

    'list resource' => 'Show subscribers list',
    'create resource' => 'Send newsletters',
];
