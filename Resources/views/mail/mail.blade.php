<html>
	<head>
		<meta charset="utf-8">
		@include("newsletter::mail.styles")
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	</head>
	<body>
		<div class="mail-nav">
			<span><b>{{ config("dk-newsletter.emailNavHeader") }}</b></span>
		</div>
		<div class="mail-content">
			<div class="mail-box mail-customMailBox">
				{!! $mailContent !!}
			</div>
		</div>
	</body>
</html>