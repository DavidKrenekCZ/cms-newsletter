<style>
			body {
				margin: 0;
				padding: 0;
				background-color: #ecf0f5;
				font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
			}

			.mail-nav {
				background-color: #3c8dbc;
				color: white;
				height: 50px;
				width: 100%;
				font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
			}

			.mail-nav span {
				height: 50px;
				display: inline-block;
				line-height: 50px;
				margin-left: 1em;
				font-size: 20px;
				font-weight: 300;
			}

			.mail-content {
				padding: 1em;
			}

			.mail-box {
				background-color: white;
				border-radius: 3px;
				border-top: 3px solid #3c8dbc;
				color: #333;
				padding: 1em;
				box-shadow: 0 1px 1px rgba(0,0,0,0.1);
				margin-bottom: 1em;
			}

			h3 {
				margin: 0;
			}

			ul {
				margin: .5em 0 1.3em;
			}

			a {
				color: #db86aa;
				text-decoration: none;
			}

			a:hover {
				text-decoration: underline;
			}

			.footer {
				padding: 1em;
				text-align: right;
			}

			.mail-customMailBox p {
				margin: 0 !important;
			}

			.mail-content .btn {
				display:inline-block;
				padding:6px 12px;
				margin-bottom:0;
				font-size:14px;
				font-weight:400;
				line-height:1.42857143;
				text-align:center;
				white-space:nowrap;
				vertical-align:middle;
				-ms-touch-action:manipulation;
				touch-action:manipulation;
				cursor:pointer;
				-webkit-user-select:none;
				-moz-user-select:none;
				-ms-user-select:none;
				user-select:none;
				background-image:none;
				border:1px solid transparent;
				border-radius:4px;
			}

			.mail-content .btn-success{
				color:#fff;
				background-color:#5cb85c;
				border-color:#4cae4c;
			}
		</style>