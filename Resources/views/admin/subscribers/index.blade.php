@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('newsletter::subscribers.title.subscribers') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('newsletter::subscribers.title.subscribers') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
        <div class="box-body grid">
            <form role="form" action="{{ route("admin.newsletter.subscriber.store") }}" method="post" class="summernote-form">
                        {{ csrf_field() }}
              <div class="form-group pad-t-1">
                            <div class="form-group">
                                <label>{{ trans("newsletter::subscribers.subject") }}:</label>
                                <input type="text" class="form-control" placeholder="{{ trans("newsletter::subscribers.subject") }}" name="subject" value="" id="mail-subject">
                            </div>
                            <div class="form-group">
                                @editor('body', trans("newsletter::subscribers.content"))
                            </div>
              </div>
                        <div class="text-center">
                            <input type="button" class="btn btn-info showPreview" value="{{ trans("newsletter::subscribers.preview") }}">&nbsp;
                            <input type="submit" class="btn btn-primary" value="{{ trans("newsletter::subscribers.send") }}">
                        </div>
            </form>
        </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
  <div class="modal fade" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">{{ trans("newsletter::subscribers.preview") }}</h4>
        </div>
        <div class="modal-body">
            <div class="mail-nav">
                <span><b>{{ config("dk-newsletter.emailNavHeader") }}</b></span>
            </div>
            <div class="mail-content"></div>
        </div>
      </div>
    </div>
  </div>
@stop

@push('js-stack')
    <script>
        $(".showPreview").click(function() {
            $('#previewModal').modal('show')
            if (!$("#previewModal .mail-content .previewBox").length)
                $("#previewModal .mail-content").prepend("<div class='mail-box previewBox'></div>");

            var html =CKEDITOR.instances.body.getData(); // html value of editor
            $("#previewModal .mail-content .previewBox").html(html);
        });
    </script>
@endpush

@push('css-stack')
    @include("newsletter::mail.styles")
@endpush
