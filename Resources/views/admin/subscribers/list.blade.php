@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('newsletter::subscribers.title.subscribers-list') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('newsletter::subscribers.title.subscribers') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body grid">
                    @if (config('laravel-newsletter.apiKey') != "")
                    <a href="{{ route("admin.newsletter.subscriber.list.update") }}" class="btn btn-success btn-flat" title="{{ trans("newsletter::subscribers.update list title") }}">
                        <i class="fa fa-refresh"></i> {{ trans("newsletter::subscribers.update list") }}
                    </a><br><br>
                    @endif
                    <table class="data-table table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans("newsletter::subscribers.title.subscribed") }}</th>
                                <th>E-mail</th>
                                <th>Odebírané kategorie</th>
                                <th>{{ trans("newsletter::subscribers.title.delivery type") }}</th>
                                <th>Akce</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($subscribers as $subscriber)
                            <tr>
                                <td data-order="{{ $subscriber->isSubscribed() ? "yes" : "no" }}">
                                    @if ($subscriber->isSubscribed())
                                        <i class="fa fa-check" style="color: green" aria-hidden="true"></i>
                                    @else
                                        <i class="fa fa-times" style="color: red" aria-hidden="true"></i>
                                    @endif
                                </td>
                                <td>{{ $subscriber->email }}</td>
                                <td>{{ implode($subscriber->categoriesNames, ", ") }}</td>
                                <td>{{ trans("newsletter::subscribers.delivery types.".$subscriber->delivery_type) }}</td>
                                <td>
                                    @if ($subscriber->isSubscribed())
                                    <a title="{{ trans("newsletter::subscribers.unsubscribe") }}" href="{{ route("admin.newsletter.subscriber.unsubscribe", ["id" => $subscriber->id]) }}" class="btn btn-primary btn-flat">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                    </a>
                                    @else
                                    <a title="{{ trans("newsletter::subscribers.subscribe") }}" href="{{ route("admin.newsletter.subscriber.subscribe-subs", ["id" => $subscriber->id]) }}" class="btn btn-primary btn-flat">
                                        <i class="fa fa-envelope-open" aria-hidden="true"></i>
                                    </a>
                                    @endif 
                                    <button data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.newsletter.subscriber.delete", ["id" => $subscriber->id]) }}" class="btn btn-danger btn-flat">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>    
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@push('js-stack')
    <script>
        $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '{{ url("/public/modules/core/js/vendor/datatables/".locale().".json") }}'
                }
            });
    </script>
@endpush