@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('newsletter::settings.title.settings') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('newsletter::settings.title.settings') }}</li>
    </ol>
@stop

@section('content')
    <form role="form" action="{{ route("admin.newsletter.setting.update") }}" method="post">
        {{ csrf_field() }}
        <div class="row">
            @if (Auth::user()->hasAccess('newsletter.settings.mailchimp'))
            <!-- MailChimp integration -->
            <div class="col-md-4">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans("newsletter::settings.title.mail chimp") }}</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body grid">
                        <div class="form-group pad-t-1">
                            <label>{{ trans("newsletter::settings.fields.apikey") }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans("newsletter::settings.fields.apikey") }}" name="MAILCHIMP_APIKEY" maxlength="255" value="{{ $vls["MAILCHIMP_APIKEY"] or '' }}">
                        </div>
                        <div class="form-group">
                            <label>{{ trans("newsletter::settings.fields.listid") }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans("newsletter::settings.fields.listid") }}" name="MAILCHIMP_ID" maxlength="255" value="{{ $vls["MAILCHIMP_ID"] or '' }}">
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <!-- Dynamic Pages -->
            @if (file_exists(app_path()."/../Modules/Dynamicpages") && Auth::user()->hasAccess('newsletter.settings.dynamicpages'))
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans("newsletter::settings.title.dynamic pages") }}</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body grid">
                        <div class="form-group pad-t-1">
                            <label for="dp_on">{{ trans("newsletter::settings.fields.dynamic pages on") }}</label><br>
                            <input id="dp_on" type="checkbox" class="flat-blue" name="DP_ON" maxlength="255" @if (isset($vls["DP_ON"]) && $vls["DP_ON"] == 1) checked="checked" @endif>
                        </div>
                        <div class="form-group">
                            {!! Form:: normalSelect("DP_WEEK", trans("newsletter::settings.fields.dynamic pages week"), $errors, trans("newsletter::settings.days"), (object)$vls) !!}
                            <input class="timepicker form-control" name="DP_WEEK_TIME" value="{{ $vls["DP_WEEK_TIME"] or "" }}">
                        </div>
                        <div class="form-group">
                            @php 
                                $daysArray = []; 
                                for ($i = 1; $i <= 31; $i++) $daysArray[$i] = $i.". ".trans("newsletter::settings.day"); 
                            @endphp
                            {!! Form:: normalSelect("DP_MONTH", trans("newsletter::settings.fields.dynamic pages month"), $errors, $daysArray, (object)$vls) !!}
                            <input class="timepicker form-control" name="DP_MONTH_TIME" value="{{ $vls["DP_MONTH_TIME"] or "" }}">
                        </div>
                    </div>
                </div>
            </div>
            @endif

        </div>
        <div class="row">
            <div class="text-center">
                <div class="col-md-2 col-md-offset-5">
                    <input type="submit" class="btn btn-block btn-success" value="{{ trans("newsletter::settings.save") }}">
                </div>
            </div>
        </div>
    </form>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop

@push("css-stack")
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" integrity="sha256-Sv44mRstp+agwCWaPO4R+KKV+Az9Cu+qS5/hNRQlHT0=" crossorigin="anonymous" />

    <style type="text/css">
        .timepicker {
            margin-top: -15px;
        }

        .bootstrap-timepicker-widget table tbody tr td input[type=text] {
            border: 0;
        }
    </style>
@endpush

@push('js-stack')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js" integrity="sha256-bmXHkMKAxMZgr2EehOetiN/paT9LXp0KKAKnLpYlHwE=" crossorigin="anonymous"></script>

    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.timepicker').timepicker({
                showMeridian: false,
                minuteStep: 5
            });

            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '@php echo Module::asset("core:js/vendor/datatables/{$locale}.json") @endphp'
                }
            });
        });
    </script>
@endpush
