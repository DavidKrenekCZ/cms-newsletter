<div class="dk-newsletter-form {{ $formClass or "" }}">
	<form action="{{ route("admin.newsletter.subscriber.subscribe") }}" method="post">
		{{ csrf_field() }}
		<span class="dk-newsletter-input-wrapper {{ $inputWrapperClass or "" }}">
			<input type="text" placeholder="{{ $inputText or "E-mail" }}" name="email" class="dk-newsletter-input {{ $inputClass or "" }}">
		</span>
		<span class="dk-newsletter-button-wrapper {{ $buttonWrapperClass or "" }}">
			<input type="submit" value="{{ $buttonText or trans("newsletter::newsletters.subscribe") }}" class="dk-newsletter-button {{ $buttonClass or "" }}">
		</span>
	</form>
</div>