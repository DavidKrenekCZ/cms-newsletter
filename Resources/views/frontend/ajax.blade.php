<script>
	window.newsletterAjaxEvents = {
		onInit: function() {
			// show loading
		},

		onDone: function() {
			// hide loading
		},

		onSuccess: function() {
			alert("Přihlášení k odběru bylo úspěšné!");
		},

		onFail: function(errorCode) {
			if (errorCode == "DB_ERROR") {
				alert("Nepodařilo se navázat komunikaci s databází, opakujte akci!");
			} else if (errorCode == "ALREADY_SUBSCRIBED") {
				alert("Tento e-mail je již k odběru přihlášen.");
			} else if (errorCode == "INVALID_EMAIL") {
				alert("Zadejte prosím platný e-mail");
			} else {
				alert("Nepodařilo se spojit se se serverem, opakujte akci!");
			}
		}
	};

	$(".dk-newsletter-form form").on("submit", function(e) {
		e.preventDefault();

		var email = $(".dk-newsletter-form .dk-newsletter-input").val();
		var token = $(".dk-newsletter-form input[type='hidden']").val();
		var url = $(this).attr("action");

		$.ajax({
			type: "POST",
			url: url,
			data: {
				_token: token,
				email: email
			}
		}).done(function(d) {
			if (d.errorCode) {
				if (d.errorCode == "NO_ERROR")
					window.newsletterAjaxEvents.onSuccess();
				else
					window.newsletterAjaxEvents.onFail(d.errorCode);
			} else
				window.newsletterAjaxEvents.onFail("UNKNOWN_ERROR");
		}).fail(function() {
			window.newsletterAjaxEvents.onFail("UNKNOWN_ERROR");
		}).always(function() {
			window.newsletterAjaxEvents.onDone();
		});

		return false;
	});
</script>