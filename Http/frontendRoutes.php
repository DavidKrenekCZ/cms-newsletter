<?php

$router->get("/newsletter/queue/cron/{maxTime?}", "Admin\QueueController@cron");
$router->get("/newsletter/dynamicpages/cron", "Admin\DeliveryController@cron");

$router->post('/newsletter-api/subscribe-user', [
    'as' => 'admin.newsletter.subscriber.subscribe',
    'uses' => 'Admin\SubscriberController@subscribe',
]);