<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/newsletter'], function (Router $router) {
    $router->bind('subscriber', function ($id) {
        return app('Modules\Newsletter\Repositories\SubscriberRepository')->find($id);
    });
    $router->get('subscribers', [
        'as' => 'admin.newsletter.subscriber.index',
        'uses' => 'SubscriberController@index',
        'middleware' => 'can:newsletter.subscribers.create'
    ]);
    $router->post('subscribers', [
        'as' => 'admin.newsletter.subscriber.store',
        'uses' => 'SubscriberController@store',
        'middleware' => 'can:newsletter.subscribers.create'
    ]);
    $router->get("subscribers-list", [
        "as" => "admin.newsletter.subscriber.list",
        "uses" => "SubscriberController@showList",
        'middleware' => 'can:newsletter.subscribers.index'
    ]);
    $router->delete("subscriber-delete/{id}", [
        "as" => "admin.newsletter.subscriber.delete",
        "uses" => "SubscriberController@deleteSubscriber",
        'middleware' => 'can:newsletter.subscribers.index'
    ]);
    $router->get("subscriber-subscribe/{id}", [
        "as" => "admin.newsletter.subscriber.subscribe-subs",
        "uses" => "SubscriberController@subscribeSubscriber",
        'middleware' => 'can:newsletter.subscribers.index'
    ]);
    $router->get("subscriber-unsubscribe/{id}", [
        "as" => "admin.newsletter.subscriber.unsubscribe",
        "uses" => "SubscriberController@unsubscribeSubscriber",
        'middleware' => 'can:newsletter.subscribers.index'
    ]);
    $router->get('subscribers-list-update', [
        "as" => "admin.newsletter.subscriber.list.update",
        "uses" => "SubscriberController@updateList",
        'middleware' => 'can:newsletter.subscribers.index'
    ]);
    
    $router->bind('setting', function ($id) {
        return app('Modules\Newsletter\Repositories\SettingRepository')->find($id);
    });
    $router->get('settings', [
        'as' => 'admin.newsletter.setting.index',
        'uses' => 'SettingController@index',
        'middleware' => 'can:newsletter.subscribers.index'
    ]);
    $router->post('settings', [
        'as' => 'admin.newsletter.setting.update',
        'uses' => 'SettingController@update',
        'middleware' => 'can:newsletter.subscribers.index'
    ]);
});