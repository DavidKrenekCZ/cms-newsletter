<?php

namespace Modules\Newsletter\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Newsletter\Entities\Delivery;
use Modules\Newsletter\Entities\Setting;
use Modules\Newsletter\Entities\Subscriber;
use Modules\Newsletter\Entities\QueueTask;
use Modules\Newsletter\Http\Requests\CreateDeliveryRequest;
use Modules\Newsletter\Http\Requests\UpdateDeliveryRequest;
use Modules\Newsletter\Repositories\DeliveryRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class DeliveryController extends AdminBaseController {
    /**
     * @var DeliveryRepository
     */
    private $delivery; 

    public function cron() {
        if (!file_exists(app_path()."/../Modules/Dynamicpages") || !Setting::get("DP_ON"))
            return "";

        if (
            (date("j") >= Setting::get("DP_MONTH") || date("j") == date("t")) &&    // Correct day or last day of month
            strtotime(date("G:i")) >= strtotime(Setting::get("DP_MONTH_TIME")) &&   // Correct time
            !Delivery::where("type", "month")                                       // Month newsletter not sent yet
                     ->whereMonth("created_at", "=", date("n"))->count()
        ) {
            $delivery = new Delivery([ "type" => "month" ]);
            $this->sendNewsletter($delivery);
        }

        if (
            date("N") >= Setting::get("DP_WEEK") &&                                 // Correct day
            strtotime(date("G:i")) >= strtotime(Setting::get("DP_WEEK_TIME")) &&    // Correct time
            !Delivery::where("type", "week")->whereBetween('created_at', [          // Week newsletter not sent yet
                \Carbon\Carbon::parse('last monday')->startOfDay(),
                \Carbon\Carbon::parse('this sunday')->endOfDay(),
            ])->count()
        ) {
            $delivery = new Delivery([ "type" => "week" ]);
            $this->sendNewsletter($delivery);
        }

        $delivery = new Delivery([ "type" => "always" ]);
        $this->sendNewsletter($delivery);
    }

    private function sendNewsletter($delivery) {
        // Calculate since when records should be sent
        $lastDelivery = Delivery::where("type", $delivery->type)->orderBy("created_at", "desc");
        if ($lastDelivery && $lastDelivery->count())
            $takeSince = strtotime($lastDelivery->first()->created_at);
        else
            $takeSince = "0";

        $records = \Modules\DynamicPages\Entities\Record::where([
            ["visible", 1], 
            ["published_since", "<", time()]
        ])->where(function ($query) use ($takeSince) {
            $query->whereNull("published_to")->orWhere("published_to", ">", $takeSince);
        })->orderBy("published_since", "desc");

        if (!$records || !$records->count())
            return false;
        $delivery->save();

        // Assemble records into an array
        $recordsArray = [];
        foreach ($records->get() as $record) {
            foreach ($record->categories as $category) {
                $cat = \Modules\DynamicPages\Entities\Category::find($category);
                if ($cat && $cat->count()) {
                    if (!isset($recordsArray[$category]))
                        $recordsArray[$category] = [ "name" => $cat->name, "records" => [] ];
                    $recordsArray[$category]["records"][] = [
                        "name" => $record->name,
                        "url" => $record->purl($category)
                    ];
                }
            }
        }

        // Subscribers we should send newsletter to
        $subscribers = Subscriber::where([ ["delivery_type", $delivery->type], ["unsubscribed_at", null] ]);
        $influenced = 0;

        foreach ($subscribers ->get() as $subscriber) {
            $subscribersRecords = [];
            foreach ($subscriber->categoriesIds as $id)
                if (isset($recordsArray[$id]))
                    $subscribersRecords[] = $recordsArray[$id];

            // Subscriber has records to send
            if (count($subscribersRecords)) {
                $influenced++;

                $task = new QueueTask;
                $task->mail = $subscriber["email"];
                $task->subject = "Novinky";

                $content = "Dobrý den, přinášíme Vám pravidelný přehled novinek.<br><br>";

                foreach ($subscribersRecords as $category) {
                    $content .= "<b>".$category["name"]."</b><ul>";
                    foreach ($category["records"] as $record)
                        $content .= "<li><a href='".$record["url"]."'>".$record["name"]."</a></li>";
                    $content .= "</ul>";
                }

                $task->content = $content;
                $task->save();
            }
        }

        if ($influenced > 0) {
            $delivery->done_at = date("Y-m-d H:i");
            $delivery->sent_to = $influenced;
            $delivery->save();
        } else
            $delivery->delete();

        return $delivery;
    }






    public function __construct(DeliveryRepository $delivery)
    {
        parent::__construct();

        $this->delivery = $delivery;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$deliveries = $this->delivery->all();

        return view('newsletter::admin.deliveries.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('newsletter::admin.deliveries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateDeliveryRequest $request
     * @return Response
     */
    public function store(CreateDeliveryRequest $request)
    {
        $this->delivery->create($request->all());

        return redirect()->route('admin.newsletter.delivery.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('newsletter::deliveries.title.deliveries')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Delivery $delivery
     * @return Response
     */
    public function edit(Delivery $delivery)
    {
        return view('newsletter::admin.deliveries.edit', compact('delivery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Delivery $delivery
     * @param  UpdateDeliveryRequest $request
     * @return Response
     */
    public function update(Delivery $delivery, UpdateDeliveryRequest $request)
    {
        $this->delivery->update($delivery, $request->all());

        return redirect()->route('admin.newsletter.delivery.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('newsletter::deliveries.title.deliveries')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Delivery $delivery
     * @return Response
     */
    public function destroy(Delivery $delivery)
    {
        $this->delivery->destroy($delivery);

        return redirect()->route('admin.newsletter.delivery.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('newsletter::deliveries.title.deliveries')]));
    }
}
