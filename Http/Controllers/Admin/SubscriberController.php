<?php

namespace Modules\Newsletter\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Newsletter\Entities\Subscriber;
use Modules\Newsletter\Http\Requests\CreateSubscriberRequest;
use Modules\Newsletter\Http\Requests\UpdateSubscriberRequest;
use Modules\Newsletter\Repositories\SubscriberRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\Mail;
use Modules\Newsletter\Entities\QueueTask;
use Spatie\Newsletter\NewsletterFacade as Newsletter;

class SubscriberController extends AdminBaseController
{
    /**
     * @var SubscriberRepository
     */
    private $subscriber;

    public function __construct(SubscriberRepository $subscriber)
    {
        parent::__construct();

        $this->subscriber = $subscriber;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view('newsletter::admin.subscribers.index', compact(''));
    }

    public function showList() {
        $subscribers = $this->subscriber->all();

        return view('newsletter::admin.subscribers.list', [
            "subscribers" => $subscribers
        ]);
    }

    public function updateList() {
        $this->updateListPrivate();

        return redirect()->route('admin.newsletter.subscriber.list')
            ->withSuccess(trans('newsletter::subscribers.updated'));
    }

    private function updateListPrivate() {
        if (config('laravel-newsletter.apiKey') == "")
            return true;
        // MailChimp integration not set
        if (config('newsletter.lists.'.config("newsletter.defaultListName").'.id') == "fooPlaceholder" || config('newsletter.apiKey') == "dummyPlaceholder")
            return false;
        $api = Newsletter::getApi(); // MailChimp API to send requests

        // get total number of subscribers
        $numberOfSubscribers = $api->get("lists/".config('newsletter.lists.'.config("newsletter.defaultListName").'.id')."/members", [
            "fields" => "total_items",  // get only total number of subscribers
            "status" => "subscribed"    // count only subscribed people
        ])["total_items"];

        // get all subscribers' emails and names from API
        $response = $api->get("lists/".config('newsletter.lists.'.config("newsletter.defaultListName").'.id')."/members", [
            "count" => $numberOfSubscribers,                                            // get all subscribers
            "fields" => "members.email_address",                                        // get only these fields
            "status" => "subscribed"                                                    // only subscribed members
        ])["members"];

        $mailChimpArray = [];
        foreach ($response as $value)
            $mailChimpArray[] = $value["email_address"];

        $databaseArray = [];
        foreach (Subscriber::all() as $value) {
            $mail = $value->email;
            $databaseArray[] = $mail;
            
            // User unsubscribed from MailChimp => delete in database
            if (!in_array($mail, $mailChimpArray))
                $value->unsubscribe();
        }

        // User deleted from database => unsubscribe from MailChimp
        foreach ($mailChimpArray as $mail)
            if (!in_array($mail, $databaseArray))
                Newsletter::unsubscribe($mail);
    }

    public function deleteSubscriber($id) {
        $subs = Subscriber::find($id);;
        if (config('laravel-newsletter.apiKey') != "")
            Newsletter::unsubscribe($subs->email);
        $subs->delete();
        return redirect()->route('admin.newsletter.subscriber.list')
            ->withSuccess(trans('newsletter::subscribers.deleted'));
    }

    public function subscribeSubscriber($id) {
        $subs = Subscriber::find($id);;
        if (config('laravel-newsletter.apiKey') != "")
            Newsletter::subscribe($subs->email);
        $subs->subscribe();
        return redirect()->route('admin.newsletter.subscriber.list')
            ->withSuccess(trans('newsletter::subscribers.subscribed'));
    }

    public function unsubscribeSubscriber($id) {
        $subs = Subscriber::find($id);;
        if (config('laravel-newsletter.apiKey') != "")
            Newsletter::unsubscribe($subs->email);
        $subs->unsubscribe();
        return redirect()->route('admin.newsletter.subscriber.list')
            ->withSuccess(trans('newsletter::subscribers.unsubscribed'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateSubscriberRequest $request
     * @return Response
     */
    public function store(Request $request) {
        $this->updateListPrivate();

        $emails = [];
        foreach (Subscriber::all() as $subscriber) { // loop through subscribers
            $task = new QueueTask;
            $task->mail = $subscriber["email"];
            $task->subject = $request["subject"];
            $task->content = $request["body"];
            $task->save();
        }

        /*
        Mail::send("newsletter::mail.mail", [
                "mailContent" => $request["content"]
            ], function ($m) use ($emails, $request) {
                    $m->from(config("dk-newsletter.sendingMail"), config("dk-newsletter.sendingName"));
                    $m->to($emails)->subject($request["subject"]);
                });
        */

        return redirect()->route('admin.newsletter.subscriber.index')
            ->withSuccess(trans('newsletter::newsletters.mail-sent'));
    }


    public function subscribe(Request $request) {
        $errorCode = "UNKNOWN_ERROR";
        if (isset($request->email)) {
            $email = trim($request->email);
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $already = Subscriber::where("email", $email)->count();
                if (!$already) {
                    $subscriber = new Subscriber;
                    $subscriber->email = $email;
                    $subscriber->unsubscribed_at = null;
                    $subscriber->delivery_type = isset($request->deliveryType) ? $request->deliveryType : "";
                    $subscriber->delivery_categories = isset($request->deliveryCategories) ? $request->deliveryCategories : "";

                    if ($subscriber->save()) {
                        if (config('laravel-newsletter.apiKey') != "")
                            Newsletter::subscribeOrUpdate($email, [], config("newsletter.defaultListName"));                        
                        $errorCode = "NO_ERROR";
                    } else
                        $errorCode = "DB_ERROR";
                } else
                    $errorCode = "ALREADY_SUBSCRIBED";
            } else
                $errorCode = "INVALID_EMAIL";
        }
        return response()->json([
            "errorCode" => $errorCode
        ]);
    }
}
