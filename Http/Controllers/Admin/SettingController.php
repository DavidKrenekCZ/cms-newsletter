<?php

namespace Modules\Newsletter\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Newsletter\Entities\Setting;
use Modules\Newsletter\Http\Requests\CreateSettingRequest;
use Modules\Newsletter\Http\Requests\UpdateSettingRequest;
use Modules\Newsletter\Repositories\SettingRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class SettingController extends AdminBaseController
{
    /**
     * @var SettingRepository
     */
    private $setting;

    public function __construct(SettingRepository $setting)
    {
        parent::__construct();

        $this->setting = $setting;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view('newsletter::admin.settings.index', [
            "vls" => Setting::allArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     */
    public function update(Request $request) {
        $post = $request->all();
        unset($post["_token"]);
        
        // Checkboxes
        foreach (["DP_ON"] as $box)
            if (!isset($post[$box]) || $post[$box] == "off")
                $post[$box] = "off";

        foreach ($post as $key => $value) {
            $setting = Setting::where("key", $key);
            if (!$setting || !$setting->count())
                $setting = new Setting;
            else
                $setting = $setting->first();

            if (trim($value) == "" && $setting->value != "")
                $setting->delete();
            else {
                if (substr($value, -5) != "*****") {
                    $setting->key = $key;
                    $setting->value = trim($value) == "" ? "" : $value;
                    $setting->save();
                }
            }
        }

        return redirect()->route('admin.newsletter.setting.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('newsletter::settings.title.settings')]));
    }
}
