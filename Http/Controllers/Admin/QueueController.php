<?php

namespace Modules\Newsletter\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use Modules\Newsletter\Entities\QueueTask;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Newsletter\Mail\QueueMail;

class QueueController extends AdminBaseController {
    public function cron($maxTime=false) {
        define("MAX_TASK_AT_ONCE", 120);
        $x = 1;

        $taskId = QueueTask::nextId();
        while ($taskId) {
            $this->sendNextTask($taskId);

            if (++$x >= MAX_TASK_AT_ONCE) die();
            $taskId = QueueTask::nextId();
        }
    }

    private function sendNextTask($taskId) {
    	$task = QueueTask::find($taskId);
    	$task->in_progress = 1;
    	$task->save();

        if (Mail::to($task->mail)->send(new QueueMail($task->subject, $task->content))) {
            $task->in_progress = 0;
        	$task->save();
        } else
        	$task->delete();
    }
}
