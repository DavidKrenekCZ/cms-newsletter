<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNewsletterSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('newsletter__subscribers', function (Blueprint $table) {
            $table->string('delivery_type');
            $table->boolean('delivery_done')->default(0);
            $table->string("delivery_categories", 1024);
            $table->dateTime('unsubscribed_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newsletter__subscribers', function (Blueprint $table) {
            $table->dropColumn(['delivery_type', 'delivery_done', "delivery_categories", 'unsubscribed_at']);
        });
    }
}
