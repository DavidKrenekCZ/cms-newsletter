<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsletterQueueTasksTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("newsletter__queue_tasks", function(Blueprint $table) {
            $table->bigIncrements("id");
            $table->string("mail", 255);
            $table->string("subject", 255);
            $table->string("content", 50000);
            $table->boolean("in_progress")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop("newsletter__queue_tasks");
    }
}
