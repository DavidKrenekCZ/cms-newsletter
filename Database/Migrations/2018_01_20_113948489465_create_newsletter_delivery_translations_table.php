<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsletterDeliveryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletter__delivery_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('delivery_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['delivery_id', 'locale']);
            $table->foreign('delivery_id')->references('id')->on('newsletter__deliveries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newsletter__delivery_translations', function (Blueprint $table) {
            $table->dropForeign(['delivery_id']);
        });
        Schema::dropIfExists('newsletter__delivery_translations');
    }
}
