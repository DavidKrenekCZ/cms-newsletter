<?php

namespace Modules\Newsletter\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterNewsletterSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('newsletter::newsletters.title.newsletters'), function (Item $item) {
                $item->icon('fa fa-envelope');
                $item->weight(10);
                $item->authorize(
                    $this->auth->hasAccess('newsletter.subscribers.create') ||
                    $this->auth->hasAccess('newsletter.subscribers.index') ||
                    $this->auth->hasAccess('newsletter.settings.mailchimp') || 
                    $this->auth->hasAccess('newsletter.settings.dynamicpages')
                );
                $item->item(trans('newsletter::subscribers.title.subscribers'), function (Item $item) {
                    $item->icon('fa fa-circle-o');
                    $item->weight(0);
                    $item->route('admin.newsletter.subscriber.index');
                    $item->authorize(
                        $this->auth->hasAccess('newsletter.subscribers.create')
                    );
                });
                $item->item(trans('newsletter::subscribers.title.subscribers-list'), function (Item $item) {
                    $item->icon('fa fa-circle-o');
                    $item->weight(0);
                    $item->route('admin.newsletter.subscriber.list');
                    $item->authorize(
                        $this->auth->hasAccess('newsletter.subscribers.index')
                    );
                });
                $item->item(trans('newsletter::settings.title.settings'), function (Item $item) {
                    $item->icon('fa fa-circle-o');
                    $item->weight(0);
                    $item->route('admin.newsletter.setting.index');
                    $item->authorize(
                        $this->auth->hasAccess('newsletter.settings.mailchimp') || 
                        $this->auth->hasAccess('newsletter.settings.dynamicpages')
                    );
                });
            
            $item->authorize(
                $this->auth->hasAccess('newsletter.subscribers.index')
            );


            });
        });

        return $menu;
    }
}
